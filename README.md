# Vision for Unified Micromagnetic Modeling (UMM) with Ubermag

Hans Fangohr<sup>1,2,3</sup>, Martin Lang<sup>1,2,3</sup>, Samuel J. R. Holt<sup>1,2</sup>, Swapneel Amit Pathak<sup>1,2</sup>, Kauser Zulfiqar<sup>1,2,4</sup>, and Marijan Beg<sup>5</sup> 

<sup>1</sup> *Max Planck Institute for the Structure and Dynamics of Matter, Luruper Chaussee 149, 22761 Hamburg, Germany*  
<sup>2</sup> *University of Southampton, Southampton SO17 1BJ, United Kingdom*  
<sup>3</sup> *Center for Free-Electron Laser Science, Luruper Chaussee 149, 22761 Hamburg, Germany*  
<sup>4</sup> *University of Hamburg, Jungiusstrasse 9, 20355 Hamburg, Germany*  
<sup>5</sup> *Department of Earth Science and Engineering, Imperial College London, London SW7 2AZ, UK*  

| Description | Badge |
| --- | --- |
| Paper | Coming soon ... |
| Binder | [![Binder](badges/binder.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fubermag%2Fpaper-2023-unified-micromagnetic-modelling/HEAD) |
| License | [![License](badges/License.svg)](https://opensource.org/licenses/BSD-3-Clause) |
| DOI | [![DOI](badges/DOI.svg)](https://doi.org/10.17617/3.D5FT5Y) |


## About

This repository provides Ubermag simulation code to complement results from
the Unified Micromagnetic Modeling manuscript. All notebooks hosted in this repository can be run in the cloud (see
next section on Binder), and thus the results reproduced by anybody.

This repository contains two main notebooks:
- **simulations_with_ubermag.ipynb**:
This notebook contains an example on how to run Ubermag simulations (Figure 3 of the paper).
- **compare_calculators.ipynb**:
This notebook outlines an example workflow of how to compare two
micromagnetic calculators using Ubermag.
This uses the same example as given in the first notebook.
The data for this comparison is stored in the folders `oommf_system` and `mumax3_system`.


## Binder

Jupyter notebooks hosted in this repository can be executed and modified in the
cloud via Binder. This does not require you to have anything installed and no
files will be created on your machine. To access Binder, click on the Binder
badge in the table above. For faster execution we recommend a local installation.
Please note that Binder does not have GPUs and therefore code which uses
Mumax3 cannot be executed.


## License

Licensed under the BSD 3-Clause "New" or "Revised" License. For details, please
refer to the [LICENSE](LICENSE) file.

## How to cite


## Acknowlegdements

- EPSRC Programme Grant on [Skyrmionics](http://www.skyrmions.ac.uk) (EP/N032128/1)
- Horizon 2020 European Research Infrastructure project [OpenDreamKit](https://opendreamkit.org/) (676541)
