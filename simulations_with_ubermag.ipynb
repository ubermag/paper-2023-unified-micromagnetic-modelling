{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Unified Micromagnetic Modelling Example\n",
    "\n",
    "In this Jupyter Notebook we will give an example of a Unified Micromagnetic Modelling (UUM) workflow using Ubermag.\n",
    "\n",
    "We want to simulate a domain wall conversion [1] in a two-dimensional thin film sample with:\n",
    "\n",
    "- exchange energy constant $A = 15 \\,\\text{pJ}\\,\\text{m}^{-1}$,\n",
    "- Dzyaloshinskii-Moriya energy constant $D = 3 \\,\\text{mJ}\\,\\text{m}^{-2}$,\n",
    "- uniaxial anisotropy constant $K = 0.5 \\,\\text{MJ}\\,\\text{m}^{-3}$ with $\\hat{\\mathbf{u}} = (0, 0, 1)$ in the out of plane direction,\n",
    "\n",
    "Geometry with discretisation cell size $(2 \\,\\text{nm}, 2 \\,\\text{nm}, 2 \\,\\text{nm})$ looks like this:\n",
    "    \n",
    "<img src=\"figures/dw_pair_conversion_geometry.png\" width=\"300\">  \n",
    "\n",
    "Simulation steps:\n",
    "- create domain wall in narrow part\n",
    "- apply spin polarised current for 0.2ns\n",
    "\n",
    "[1] Zhou, Y., & Ezawa, M. (2014). A reversible conversion between a skyrmion and a domain-wall pair in a junction geometry. *Nature Communications* **5**, 8. https://doi.org/10.1038/ncomms5652\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# define micromagnetic problem\n",
    "import micromagneticmodel as mm\n",
    "\n",
    "# define material parameters\n",
    "Ms = 5.8e5  # saturation magnetisation (A/m)\n",
    "A = 15e-12  # exchange energy constant (J/)\n",
    "D = 3e-3  # Dzyaloshinkii-Moriya energy constant (J/m**2)\n",
    "K = 0.5e6  # uniaxial anisotropy constant (J/m**3)\n",
    "u = (0, 0, 1)  # easy axis\n",
    "gamma0 = 2.211e5  # gyromagnetic ratio (m/As)\n",
    "alpha = 0.3  # Gilbert damping\n",
    "\n",
    "# define Hamiltonian and dynamics\n",
    "system = mm.System(name='dw_pair_conversion')\n",
    "system.energy = mm.Exchange(A=A) + mm.DMI(D=D, crystalclass='Cnv_z') + mm.UniaxialAnisotropy(K=K, u=u)\n",
    "system.dynamics = mm.Precession(gamma0=2.211e5) + mm.Damping(alpha=alpha)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# define geometry and discretisation\n",
    "import discretisedfield as df\n",
    "\n",
    "p1 = (0, 0, 0)                   # corner points of rectangle\n",
    "p2 = (150e-9, 50e-9, 2e-9)\n",
    "cell = (2e-9, 2e-9, 2e-9)        # discretisation cell size 2nm\n",
    "\n",
    "region = df.Region(p1=p1, p2=p2)\n",
    "mesh = df.Mesh(region=region, cell=cell)\n",
    "\n",
    "# set Ms to zero where there is no magnetic material\n",
    "def Ms_fun(pos):\n",
    "    x, y, z = pos\n",
    "    if x < 50e-9 and (y < 15e-9 or y > 35e-9):\n",
    "        return 0\n",
    "    else:\n",
    "        return Ms\n",
    "    \n",
    "# define initial magnetisation field \n",
    "def m_init(pos):\n",
    "    x, y, z = pos\n",
    "    if 30e-9 < x < 40e-9:\n",
    "        return (0.1, 0.1, -1)\n",
    "    else:\n",
    "        return (0.1, 0.1, 1)\n",
    "\n",
    "system.m = df.Field(mesh, nvdim=3, value=m_init, norm=Ms_fun, valid=\"norm\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "system.m.z.sel(z=1e-09).mpl.scalar()    # visualise m_z component of initial magnetisation at z=1nm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# activate the OOMMF micromagnetic Calculator (oc)\n",
    "import oommfc as calc  # We could change `oommfc` to `mumax3c` to change the calculator\n",
    "md = calc.MinDriver()\n",
    "md.drive(system)  # actual execution of micromagnetic energy minimisation\n",
    "\n",
    "system.m.z.sel(z=1e-9).mpl.scalar()  # plot m_z at z=1nm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# add spin torque term to dynamics of system\n",
    "ux = 400  # velocity in x direction (m/s)\n",
    "beta = 0.5  # non-adiabatic STT parameter\n",
    "\n",
    "system.dynamics = system.dynamics + mm.ZhangLi(u=ux, beta=beta)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "td = calc.TimeDriver()\n",
    "td.drive(system, t=0.2e-9, n=200)  # compute time development for 0.2ns\n",
    "\n",
    "system.m.z.sel(z=1e-9).mpl.scalar()  # plot m_z at z=1nm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [],
   "source": [
    "import micromagneticdata as md  # analyse all of the data files that have been written\n",
    "data = md.Data(system.name)\n",
    "time_drive = data[-1]  # Use only the last time drive (with SST term)\n",
    "\n",
    "# fine tune visualisation: put slider below plot\n",
    "import holoviews as hv\n",
    "hv.extension('bokeh')\n",
    "hv.output(widget_location=\"bottom\")\n",
    "\n",
    "# create interactive plot with holoviews\n",
    "time_drive.hv(\n",
    "    kdims=[\"x\", \"y\"],\n",
    "    scalar_kw={\"clim\": (-Ms, Ms), \"cmap\": \"coolwarm\"},\n",
    ").opts(frame_width=1200)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.17"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
